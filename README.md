# dnsdig

## Description

A little script using `dig`, `host`, `drill` or `resolvectl` in sequence of priority to measure response times of DNS resolvers.

- ⚠
Currently if `resolvectl` is used, it uses the system-wide configured DNS's and not the provided external IP's to query the domain.

## Usage

### Local

Save, mark executable, run.

```shell
curl -O https://gitlab.com/TriMoon/dnsdig/-/raw/main/dnsdig
chmod +x dnsdig
./dnsdig
```

##### Examples with watch

To monitor changes over time, it is handy to run the script using [watch(1)] in a separate terminal. 😉

```shell
watch -bcex ./dnsdig
watch -bcexd -n 30 ./dnsdig
```

### Remote

The script can be used directly from [this source][online source].

```shell
bash <(curl -s PUT_URL_HERE)
```

### Options

A few extra options are avilable in the form of environment variables.

- `COLUMNS=x`<br/>
Can be used to set the line width of the output.<br/>
_(Defaults to the max line length of the terminal.)_

- `DOMAIN=any.domain.name`<br/>
Can be used to change the domain used for lookups.<br/>
_Defaults to the FQDN of `HOME_URL` in `/etc/os-release`_<br/>
_(Will be made a commandline option...)_

- <s>`TESTDNS=X.Y.Y.Z`<br/>
Can be used to test resolver IP(s) not already pre-configured.</s><br/>
_(Is currently only used when set/enabled inside the script, but will be made a commandline option...)<br/>
Defaults to `127.0.0.1 ::1 127.0.0.53`_

## Pre-Configured DNS-Servers

These can be viewed by:

- Running the script while `showServers=1` is set.<br/>
_This is currently only setable inside the script, but will change in future with options_
- Viewing the [Pre-Configured DNS-Servers.html](Pre-Configured DNS-Servers.html) file.<br/>
_This is still a W.I.P._

[watch(1)]:			https://man7.org/linux/man-pages/man1/watch.1.html "man watch"
[online source]:	https://gitlab.com/TriMoon/dnsdig/-/raw/main/dnsdig "Link to the latest version"
[manjaro.org]:		https://manjaro.org